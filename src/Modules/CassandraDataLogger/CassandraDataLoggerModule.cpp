/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

/// \cond
#include <chrono>
#include <sstream>
/// \endcond

#include "CassandraDataLoggerModule.hpp"
#include "Utils/Common.hpp"
#include "Utils/Hash.hpp"
#include "Utils/Logging.hpp"

using namespace std::chrono_literals;
namespace daqutils = daqling::utilities;

CassandraDataLoggerModule::CassandraDataLoggerModule()
{
  INFO(" setting up session.");
  m_cluster = cass_cluster_new();
  m_session = cass_session_new();
  setup();
}

CassandraDataLoggerModule::~CassandraDataLoggerModule()
{
  INFO(" Closing session and connection to cluster...");
  CassFuture *future = cass_session_close(m_session);
  cass_future_wait(future);
  cass_future_free(future);
  cass_cluster_free(m_cluster);
  INFO(" Cassandra connection closed.");
}

void CassandraDataLoggerModule::readIntoBinary(daqutils::Binary &binary,
                                               const CassValue *const &value)
{
  const cass_byte_t *value_bytes;
  size_t value_length;
  cass_value_get_bytes(value, &value_bytes, &value_length);
  binary = daqutils::Binary(static_cast<const void *>(value_bytes), value_length);
}

const std::string CassandraDataLoggerModule::getErrorStr(CassFuture *&future)
{
  CassError rc = cass_future_error_code(future);
  std::string errStr(cass_error_desc(rc));
  const char *message;
  size_t message_length;
  cass_future_error_message(future, &message, &message_length);
  std::string mssgStr(message, message_length);
  std::string finalStr = errStr + "  Cassandra Message: " + mssgStr;
  return finalStr;
}

bool CassandraDataLoggerModule::prepareQuery(const std::string &qStr, const CassPrepared **prepared)
{
  bool success = false;
  CassFuture *future = cass_session_prepare(m_session, qStr.c_str());
  cass_future_wait(future);
  if (cass_future_error_code(future) != CASS_OK) {
    ERROR(" Query preparation failed for: " << qStr << " CassError: " << getErrorStr(future));
    return success;
  } else {
    *prepared = cass_future_get_prepared(future);
    success = true;
  }
  cass_future_free(future);
  return success;
}

bool CassandraDataLoggerModule::executeStatement(CassStatement *&statement)
{
  bool success = false;
  CassFuture *future = cass_session_execute(m_session, statement);
  cass_future_wait(future);
  if (cass_future_error_code(future) != CASS_OK)
    ERROR(" Statement execution failed. CassError: " << getErrorStr(future));
  else
    success = true;
  cass_future_free(future);
  return success;
}

bool CassandraDataLoggerModule::executeStatement(CassStatement *&statement,
                                                 const CassResult **result)
{
  bool success = false;
  CassFuture *future = cass_session_execute(m_session, statement);
  cass_future_wait(future);
  if (cass_future_error_code(future) != CASS_OK) {
    ERROR(" Statement execution failed. CassError: " << getErrorStr(future));
  } else {
    *result = cass_future_get_result(future);
    success = true;
  }
  cass_future_free(future);
  return success;
}

bool CassandraDataLoggerModule::executeQuery(const std::string &queryStr)
{
  bool success = false;
  CassStatement *statement = cass_statement_new(queryStr.c_str(), 0);
  CassFuture *future = cass_session_execute(m_session, statement);
  cass_future_wait(future);
  if (cass_future_error_code(future) != CASS_OK)
    ERROR(" Query execution failed for: " << queryStr << " CassError: " << getErrorStr(future));
  else
    success = true;
  cass_future_free(future);
  cass_statement_free(statement);
  return success;
}

bool CassandraDataLoggerModule::columnFamilyExists(const std::string &columnFamilyName)
{
  bool found = false;
  const CassPrepared *prepared = nullptr;
  // std::string qStr = "SELECT columnfamily_name FROM system.SCHEMA_COLUMNFAMILIES WHERE
  // keyspace_name=? AND columnfamily_name=?";
  std::string qStr = Q_CF_EXISTS;
  if (prepareQuery(qStr, &prepared)) {
    CassStatement *statement = cass_prepared_bind(prepared);
    cass_statement_bind_string(statement, 0, M_KEYSPACE_NAME.c_str());
    cass_statement_bind_string(statement, 1, columnFamilyName.c_str());

    const CassResult *result = nullptr;
    if (executeStatement(statement, &result)) {
      CassIterator *iterator = cass_iterator_from_result(result);
      found = (cass_iterator_next(iterator)) ? true : false;
      cass_iterator_free(iterator);
    }
    cass_result_free(result);
    cass_statement_free(statement);
  }
  cass_prepared_free(prepared);
  return found;
}

bool CassandraDataLoggerModule::exists()
{
  return columnFamilyExists(M_CF_NAME) && m_chunkProvider.exists();
}

bool CassandraDataLoggerModule::create()
{
  /*
    std::stringstream qssChunk;
    qssChunk << "CREATE TABLE " << KEYSPACE_NAME<< "." << M_NAME
             << " (" << M_COLUMN_COMPKEY   << " text, "
                     << M_COLUMN_DATA      << " blob, "
                     << M_COLUMN_OBJSIZE   << " bigint, "
                     << M_COLUMN_CHSIZE    << " bigint, "
                     << M_COLUMN_CHCOUNT   << " int, "
                     << M_COLUMN_EXP       << " int, "
                     << M_COLUMN_ATTR      << " text, "
             << " PRIMARY KEY (" << M_COLUMN_COMPKEY << "));";
    m_cs->executeQuery( qssChunk.str() );

    std::stringstream qssChunkMeta;
    qssChunkMeta << "CREATE TABLE conddb." << M_META_NAME
                 << " (" << M_META_COLUMN_OBJNAME << " text, "
                         << M_COLUMN_OBJSIZE      << " bigint, "
                         << M_COLUMN_CHSIZE       << " bigint, "
                         << M_COLUMN_CHCOUNT      << " int, "
                         << M_META_COLUMN_TTL     << " bigint, "
                         << M_META_COLUMN_PPATH   << " text, "
                         << M_META_COLUMN_ATTR    << " text, "
                 << " PRIMARY KEY (" << M_META_COLUMN_OBJNAME << "));";
    m_cs->executeQuery( qssChunkMeta.str() );
  */

  if (exists()) {
    WARNING(" ColumnFamily exists! Won't recreate payload CF.");
    return false;
  }
  std::stringstream qss;
  qss << "CREATE TABLE " << M_KEYSPACE_NAME << "." << M_CF_NAME;
#ifdef EVID_MODE
  qss << " (" << M_COLUMN_KEY << " bigint, ";
#endif
#ifdef HASH_MODE
  qss << " (" << M_COLUMN_KEY << " text, ";
#endif
  qss << M_COLUMN_TYPE << " text, " << M_COLUMN_SINFO << " blob, " << M_COLUMN_VERSION << " text, "
      << M_COLUMN_TIME << " bigint, " << M_COLUMN_SIZE << " bigint, " << M_COLUMN_DATA << " blob, "
      << " PRIMARY KEY (" << M_COLUMN_KEY << "));";
  executeQuery(qss.str());
  m_chunkProvider.create();
  return true;
}

#warning RS -> YOU NEED TO INTRODUCE A PROPER SESSION LAYER BETWEEN STORAGE AND DAQ!

void CassandraDataLoggerModule::start()
{
  DAQProcess::start();
  INFO(" getState: " << getState());
}

void CassandraDataLoggerModule::stop()
{
  DAQProcess::stop();
  INFO(" getState: " << this->getState());
}

void CassandraDataLoggerModule::runner()
{
  INFO(" Running...");
  uint64_t incr = 0;
  while (m_run) {
    incr++;
    daqutils::Binary pl(0);
    while (!m_connections.get(1, std::ref(pl))) {
      std::this_thread::sleep_for(1ms);
    }
    write(incr, pl);
    DEBUG("Wrote data from channel 1...");
  }
  INFO(" Runner stopped");
}

void CassandraDataLoggerModule::setup()
{
  INFO(" Connecting to storage cluster based on configuration.");
  std::string clusterStr = m_config.getConfig()["settings"]["ring"];

  INFO(" -> connecting to ring: " << clusterStr);
  cass_cluster_set_write_bytes_high_water_mark(m_cluster,
                                               10485760); // Write bytes water mark set to 10MByte

  cass_cluster_set_contact_points(m_cluster, clusterStr.c_str());
  cass_cluster_set_token_aware_routing(m_cluster, cass_true);

  CassFuture *future = cass_session_connect(m_session, m_cluster);
  cass_future_wait(future);
  if (cass_future_error_code(future) != CASS_OK) {
    ERROR(" Unable to connect to Cassandra cluster for: " << clusterStr
                                                          << " CassError:" << getErrorStr(future));
  } else {
    INFO(" *wink wink* ");
  }
  INFO(" Checking PAYLOAD ColumnFamily existence...");
  if (exists()) {
    INFO("   -> CF is up.");
  } else {
    bool done = create();
    INFO("   -> CF created -> " << done);
  }
  cass_future_free(future);
}

void CassandraDataLoggerModule::write() {}

void CassandraDataLoggerModule::read() {}

bool CassandraDataLoggerModule::write(uint64_t keyId, daqling::utilities::Binary &payload)
{
  daqutils::Binary sinfoData(0);
  bool success = false;
  const CassPrepared *prepared = nullptr;
  if (prepareQuery(Q_INSERT, &prepared)) {
    CassStatement *statement = cass_prepared_bind(prepared);
#ifdef EVID_MODE
    cass_statement_bind_int64(statement, 0, keyId);
#endif
#ifdef HASH_MODE
    daqling::persistency::Hash payloadHash = daqling::persistency::makeHash("eventType", payload);
    cass_statement_bind_string(statement, 0, payloadHash.c_str());
#endif
    cass_statement_bind_string(statement, 1, "event");
    cass_statement_bind_string(statement, 3, "dummy");
    cass_statement_bind_int64(statement, 4, 0L);
    cass_statement_bind_bytes(
      statement, 2, static_cast<const unsigned char *>(sinfoData.data()), sinfoData.size());
#ifndef USE_CHUNKS
    cass_statement_bind_bytes(
      statement, 6, static_cast<const unsigned char *>(payload.data()), payload.size());
#else
    // cond::Binary dummyPayload = cond::Binary();
    // cass_statement_bind_bytes( statement, 6, reinterpret_cast<const unsigned
    // char*>(dummyPayload.data()), dummyPayload.size() ); ObjectMetadata meta =
    // ChunkedStorage::newWriter(m_chunkSP, payloadHash, payloadData).call();
#endif
    cass_statement_bind_int64(statement, 5, payload.size());
    success = executeStatement(statement);
    cass_statement_free(statement);
    success = true;
  }
  cass_prepared_free(prepared);
  return success;
}

void CassandraDataLoggerModule::shutdown() {}
